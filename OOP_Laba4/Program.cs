﻿using System;
using System.Linq;
using WorkWithArray;
struct Money : IComparable<Money>
{
    public int Dollars { get; }
    public short Cents { get; }

    public Money(int dollars, short cents)
    {
        if (dollars < 0 || cents < 0 || cents >= 100)
            throw new ArgumentException("Не валідне значення для грошей");

        Dollars = dollars;
        Cents = cents;
    }

    public Money(decimal amount) : this((int)Math.Floor(amount), (short)((amount - Math.Floor(amount)) * 100))
    {
    }

    public Money(string moneyString)
    {
        if (string.IsNullOrWhiteSpace(moneyString))
            throw new ArgumentException("Не валідний формат для грошей.");

        moneyString = moneyString.Trim();
        string[] parts = moneyString.Split('.');

        if (parts.Length != 2 || !int.TryParse(parts[0], out int dollars) || !short.TryParse(parts[1], out short cents) || dollars < 0 || cents < 0 || cents >= 100)
            throw new ArgumentException("Не валідний формат для грошей.");

        Dollars = dollars;
        Cents = cents;
    }

    public decimal ToDecimal()
    {
        return Dollars + (decimal)Cents / 100;
    }

    public override string ToString()
    {
        return $"{Dollars}.{Cents:D2}";
    }

    public int CompareTo(Money other)
    {
        decimal thisValue = this.ToDecimal();
        decimal otherValue = other.ToDecimal();
        return thisValue.CompareTo(otherValue);
    }

    public static Money operator +(Money money1, Money money2)
    {
        decimal total = money1.ToDecimal() + money2.ToDecimal();
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static Money operator -(Money money1, Money money2)
    {
        decimal total = money1.ToDecimal() - money2.ToDecimal();
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static Money operator *(Money money1, Money money2)
    {
        decimal total = money1.ToDecimal() * money2.ToDecimal();
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static Money operator /(Money money1, Money money2)
    {
        if (money2.ToDecimal() == 0)
        {
            throw new DivideByZeroException("Ділення на нуль не можливе.");
        }

        decimal total = money1.ToDecimal() / money2.ToDecimal();
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static Money operator *(Money money, int multiplier)
    {
        decimal total = money.ToDecimal() * multiplier;
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static Money operator /(Money money, int divisor)
    {
        if (divisor == 0)
        {
            throw new DivideByZeroException("Ділення на нуль не можливе.");
        }

        decimal total = money.ToDecimal() / divisor;
        int dollars = (int)total;
        short cents = (short)((total - dollars) * 100);
        return new Money(dollars, cents);
    }

    public static bool operator ==(Money money1, Money money2)
    {
        return money1.Dollars == money2.Dollars && money1.Cents == money2.Cents;
    }

    public static bool operator !=(Money money1, Money money2)
    {
        return !(money1 == money2);
    }

    public static bool operator >(Money money1, Money money2)
    {
        return money1.ToDecimal() > money2.ToDecimal();
    }

    public static bool operator <(Money money1, Money money2)
    {
        return money1.ToDecimal() < money2.ToDecimal();
    }

    public static bool operator >=(Money money1, Money money2)
    {
        return money1.ToDecimal() >= money2.ToDecimal();
    }

    public static bool operator <=(Money money1, Money money2)
    {
        return money1.ToDecimal() <= money2.ToDecimal();
    }

    public override bool Equals(object obj)
    {
        if (obj is Money other)
            return this == other;

        return false;
    }

    public override int GetHashCode()
    {
        return Dollars.GetHashCode() ^ Cents.GetHashCode();
    }
}


