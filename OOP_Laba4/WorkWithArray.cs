﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithArray
{
    class Program
    {
        static void Main()
        {
            Money[] moneyArray = ReadMoneyArrayFromConsole();
            DisplayMoneyArray(moneyArray);
            SortMoneyArray(moneyArray);
            Console.WriteLine("Відсортований масив грошей:");
            DisplayMoneyArray(moneyArray);

            Money moneyToModify = moneyArray[0];
            ModifyMoney(ref moneyToModify);
            Console.WriteLine("Модифіковані гроші:");
            Console.WriteLine(moneyToModify);

            Money minMoney, maxMoney;
            FindMinMaxMoney(moneyArray, out minMoney, out maxMoney);
            Console.WriteLine("Мінімальні гроші: " + minMoney);
            Console.WriteLine("Максимальні гроші: " + maxMoney);
        }

        static Money[] ReadMoneyArrayFromConsole()
        {
            Console.Write("Введи кількість грошових сум: ");
            int n = int.Parse(Console.ReadLine());
            Money[] moneyArray = new Money[n];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Введи грошову суму {i + 1} (долари.центи):");
                string input = Console.ReadLine();
                moneyArray[i] = new Money(input);
            }
            return moneyArray;
        }

        static void DisplayMoneyArray(Money[] moneyArray)
        {
            Console.WriteLine("Масив грошей:");
            foreach (Money money in moneyArray)
            {
                Console.WriteLine(money);
            }
        }

        static void SortMoneyArray(Money[] moneyArray)
        {
            Array.Sort(moneyArray);
        }

        static void ModifyMoney(ref Money money)
        {
            Console.WriteLine("Введи нове значення для грошей (долари.центи):");
            string input = Console.ReadLine();
            money = new Money(input);
        }

        static void FindMinMaxMoney(Money[] moneyArray, out Money minMoney, out Money maxMoney)
        {
            minMoney = moneyArray.Min();
            maxMoney = moneyArray.Max();
        }
    }
}
